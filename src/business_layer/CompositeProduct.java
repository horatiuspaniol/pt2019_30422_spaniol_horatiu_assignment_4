package business_layer;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem
{
	private ArrayList<MenuItem> menuItems;
	// name
	// price
	private boolean isPriceCorrect = true;
	
	public CompositeProduct(String name)
	{
		menuItems = new ArrayList<MenuItem>();
		this.setName(name);
		this.setPrice(0);
	}
	
	public ArrayList<MenuItem> getMenuItems()
	{
		return this.menuItems;
	}
	
	public void add(MenuItem menuItem)
	{		
		menuItems.add(menuItem);
		this.isPriceCorrect = false;
	}
	
	public boolean getIsPriceCorrect()
	{
		return this.isPriceCorrect;
	}
	
	public int computePrice()
	{
		if(isPriceCorrect == false)
		{
			for(MenuItem m : this.getMenuItems())
			{
				String menuItemType = m.getClass().getSimpleName();
				if(menuItemType.equals("BaseProduct"))
				{
					this.setPrice(this.getPrice() + m.getPrice());
				}
				else if(menuItemType.equals("CompositeProduct"))
				{
					if(((CompositeProduct) m).getIsPriceCorrect() == true)
					{
						this.setPrice(this.getPrice() + m.getPrice());
					}
					else
					{
						((CompositeProduct) m).computePrice();						
					}
				}
			}
		}
		
		isPriceCorrect = true;
		return this.getPrice();
	}
	
	
	
	
}
