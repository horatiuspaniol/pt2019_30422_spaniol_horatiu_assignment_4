package business_layer;

public class BaseProduct extends MenuItem
{
	public BaseProduct(String name, int price)
	{
		this.setName(name);
		this.setPrice(price);
	}
	
}
