package business_layer;

public interface IRestaurantProcessing
{
	void addBaseProduct();
	void editBaseProduct();

	void createCompositeProduct();
	void addCompositeProduct();
	void finalizeCompositeProduct();
	void editCompositeProduct();
	
	void deleteMenuItem();
	
	void addOrder();
	void addMenuItemToOrder();
	
	void createBill();
}
