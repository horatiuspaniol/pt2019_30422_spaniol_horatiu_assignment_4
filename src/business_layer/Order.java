package business_layer;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

public class Order implements Serializable//, Observer
{
	private int id;
	private String date;
	private int table;

	public Order(int id, String date, int table)
	{
		this.id = id;
		this.date = date;
		this.table = table;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getDate()
	{
		return date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public int getTable()
	{
		return table;
	}

	public void setTable(int table)
	{
		this.table = table;
	}

	// Automatically generated.
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + id;
		result = prime * result + table;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (date == null)
		{
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id != other.id)
			return false;
		if (table != other.table)
			return false;
		return true;
	}

//	@Override
//	public void update(Observable o, Object arg)
//	{
//		String message = "Added " + arg + " to order " + this.getId() + ".";
//		
//		JOptionPane.showMessageDialog(null, message, "Notification", JOptionPane.INFORMATION_MESSAGE);
//		
//	}


	
	
}
