package business_layer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.swing.JOptionPane;

import data_layer.RestaurantSerializator;
import presentation_layer.AdministratorGUI;
import presentation_layer.ChefGUI;

import presentation_layer.WaiterGUI;

import static java.util.stream.Collectors.toList;

public class Restaurant extends Observable implements IRestaurantProcessing
{
	static HashMap<Order, ArrayList<MenuItem>> map;
	ArrayList<MenuItem> menu;
	
	
	public Restaurant()
	{
		map = RestaurantSerializator.deserializeRestaurantFrom("restaurant.ser");
		menu = RestaurantSerializator.deserializeListFrom("menu.ser");
	}
	
	public HashMap<Order, ArrayList<MenuItem>> getRestaurant()
	{
		return map;
	}
	
	public void setRestaurant(HashMap<Order, ArrayList<MenuItem>> orderInformation)
	{
		this.map = orderInformation;
	}
	
	public ArrayList<MenuItem> getMenu()
	{
		return this.menu;
	}
	
	public ArrayList<Order> getOrders()
	{
		return (ArrayList<Order>) map.keySet().stream().collect(toList());
	}
	
	public void addMenuItemToOrder(MenuItem menuItem, Order order)
	{
		if(!map.containsKey(order))
		{
			ArrayList<MenuItem> newMenu = new ArrayList<MenuItem>();
			newMenu.add(menuItem);
			map.put(order, newMenu);	

		}
		else if(map.containsKey(order))
		{
			ArrayList<MenuItem> existingMenu = map.get(order);
			existingMenu.add(menuItem);
		}	
		Restaurant.serialize();
	}
	
	public void finalizeOrder()
	{
		setChanged();
	}
	
	public static void serialize()
	{
		RestaurantSerializator.serialize(map, "restaurant.ser");
	}

	public static HashMap<Order, ArrayList<MenuItem>> deserialize()
	{
		return RestaurantSerializator.deserializeRestaurantFrom("restaurant.ser");
	}
	
	
	private boolean isMapWellFormed(Order order)
	{
		if(order == null)
		{
			return false;
		}
		
		ArrayList<MenuItem> menu = map.get(order);
		
		for(MenuItem m : menu)
		{
			if(m == null)
			{
				return false;
			}
		}
		
		return true;
	}
	
	private boolean isOrderWellFormed(Order order)
	{
		if(order == null)
		{
			return false;
		}
		
		return true;
	}
	
	
	private boolean isMenuWellFormed(MenuItem m)
	{
		if(m == null)
		{
			return false;
		}
		
		for(MenuItem menuItem : this.menu)
		{
			if(menuItem == null)
			{
				return false;
			}
		}
		
		return true;
	}


	@Override
	public void addBaseProduct()
	{
		String tempName = AdministratorGUI.baseProductNameTextField.getText();
		int tempPrice = Integer.parseInt(AdministratorGUI.baseProductPriceTextField.getText());
		MenuItem tempMenuItem = new BaseProduct(tempName, tempPrice);
		
		assert isMenuWellFormed(tempMenuItem);
		
		menu.add(tempMenuItem);
		
		assert isMenuWellFormed(tempMenuItem);
		
		RestaurantSerializator.serialize(menu, "menu.ser");	
	}

	@Override
	public void editBaseProduct()
	{
		int selectedRow = AdministratorGUI.menuTable.getSelectedRow();
		String nameOfSelectedMenuItem = AdministratorGUI.menuTable.getValueAt(selectedRow, 0).toString();
		
		String newName = nameOfSelectedMenuItem;
		int newPrice = (int)AdministratorGUI.menuTable.getValueAt(selectedRow, 1);
		
		if(!AdministratorGUI.baseProductNameTextField.getText().trim().equals(""))
		{
			newName = AdministratorGUI.baseProductNameTextField.getText();
		}
		
		if(!AdministratorGUI.baseProductPriceTextField.getText().trim().equals(""))
		{
			newPrice = Integer.parseInt(AdministratorGUI.baseProductPriceTextField.getText());
		}
		
		
		for(MenuItem m : menu)
		{
			if(m.getName().equals(nameOfSelectedMenuItem))
			{
				m.setName(newName);
				m.setPrice(newPrice);
				assert isMenuWellFormed(m);
			}
		}
		
		
		RestaurantSerializator.serialize(menu, "menu.ser");
		
	}

	@Override
	public void createCompositeProduct()
	{
		String name = AdministratorGUI.compositeProductNameTextField.getText();
		MenuItem tempMenuItem = new CompositeProduct(name);
		
		menu.add(tempMenuItem);
		
		assert isMenuWellFormed(tempMenuItem);
		
		RestaurantSerializator.serialize(menu, "menu.ser");	
	}

	@Override
	public void addCompositeProduct()
	{
		int selectedRow = AdministratorGUI.menuTable.getSelectedRow();
		String nameOfSelectedMenuItem = AdministratorGUI.menuTable.getValueAt(selectedRow, 0).toString();
		
		MenuItem selectedMenuItem = null;
		for(MenuItem m : menu)
		{
			if(m.getName().equals(nameOfSelectedMenuItem))
			{
				selectedMenuItem = m;
				assert isMenuWellFormed(m);
			}
		}
		

		String nameOfCompositeProduct = AdministratorGUI.compositeProductNameTextField.getText();
		for(MenuItem m : menu)
		{
			if(m.getName().equals(nameOfCompositeProduct))
			{
				((CompositeProduct) m).add(selectedMenuItem);
				assert isMenuWellFormed(m);
			}
		}
		
		RestaurantSerializator.serialize(menu, "menu.ser");
		
		JOptionPane.showMessageDialog(null, "Added " + nameOfSelectedMenuItem + " to " + nameOfCompositeProduct + ".", "Success", JOptionPane.INFORMATION_MESSAGE);
		
	}

	@Override
	public void finalizeCompositeProduct()
	{
		int selectedRow = AdministratorGUI.menuTable.getSelectedRow();
		String nameOfSelectedMenuItem = AdministratorGUI.menuTable.getValueAt(selectedRow, 0).toString();
	
		for(MenuItem m : menu)
		{
			if(m.getName().equals(nameOfSelectedMenuItem))
			{
				((CompositeProduct) m).computePrice();
				assert isMenuWellFormed(m);
			}
		}
		
		RestaurantSerializator.serialize(menu, "menu.ser");
	}

	@Override
	public void editCompositeProduct()
	{
		int selectedRow = AdministratorGUI.menuTable.getSelectedRow();
		String nameOfSelectedMenuItem = AdministratorGUI.menuTable.getValueAt(selectedRow, 0).toString();
	
		String newName = nameOfSelectedMenuItem;
		
		if(!AdministratorGUI.compositeProductNameTextField.getText().trim().equals(""))
		{
			newName = AdministratorGUI.compositeProductNameTextField.getText();
		}
		
		for(MenuItem m : menu)
		{
			if(m.getName().equals(nameOfSelectedMenuItem))
			{
				m.setName(newName);
				assert isMenuWellFormed(m);
			}
		}
		
		RestaurantSerializator.serialize(menu, "menu.ser");
	}

	@Override
	public void deleteMenuItem()
	{
		int selectedRow = AdministratorGUI.menuTable.getSelectedRow();
		String nameOfSelectedMenuItem = AdministratorGUI.menuTable.getValueAt(selectedRow, 0).toString();

		Iterator iterator = menu.iterator();
		while(iterator.hasNext())
		{
			MenuItem m = (MenuItem) iterator.next();
			if(m.getName().equals(nameOfSelectedMenuItem))
			{
				iterator.remove();
			}
			
		}
		
		RestaurantSerializator.serialize(menu, "menu.ser");
	}

	@Override
	public void addOrder()
	{
		int readId = Integer.parseInt(WaiterGUI.idTextField.getText());
		String readDate = WaiterGUI.dateTextField.getText();
		int readTable = Integer.parseInt(WaiterGUI.tableTextField.getText());
		
		Order o = new Order(readId, readDate, readTable);
		ArrayList<MenuItem> associatedMenuItemList = new ArrayList<MenuItem>();
		
		//orders.add(o);
		map.put(o, new ArrayList<MenuItem>());		
		isOrderWellFormed(o);
		
		RestaurantSerializator.serialize(this.getOrders(), "orders.ser");
	}

	@Override
	public void addMenuItemToOrder()
	{
		// Select order
		int selectedRow = WaiterGUI.orderTable.getSelectedRow();
		int idOfSelectedOrder = (int)WaiterGUI.orderTable.getValueAt(selectedRow,0);
		
		Order selectedOrder = null;
		
		for(Order o : map.keySet())
		{
			if(o.getId() == idOfSelectedOrder)
			{	
				selectedOrder = o;
			}
		}
		assert isMapWellFormed(selectedOrder);
		
		// Select product
		String nameOfDesiredProduct = WaiterGUI.menuItemNameTextField.getText();	
		MenuItem selectedMenuItem = null;
		
		for(MenuItem m : menu)
		{
			if(m.getName().equals(nameOfDesiredProduct))
			{
				selectedMenuItem = m;
			}
		}
		
		assert isMenuWellFormed(selectedMenuItem);
		
		this.addMenuItemToOrder(selectedMenuItem, selectedOrder);
		
		String message = "Added " + selectedMenuItem.getName() + " to order " + selectedOrder.getId() + ".";
		
		JOptionPane.showMessageDialog(null, message, "Notification", JOptionPane.INFORMATION_MESSAGE);
		
		assert isMapWellFormed(selectedOrder);
	}

	@Override
	public void createBill()
	{
		int selectedRow = WaiterGUI.orderTable.getSelectedRow();
		int selectedOrderId = (int)WaiterGUI.orderTable.getValueAt(selectedRow, 0);
		Order selectedOrder = null;
		
		for(Order o : map.keySet())
		{
			if(o.getId() == selectedOrderId)
			{
				selectedOrder = o;
			}
		}
		
		assert isMapWellFormed(selectedOrder);
		
		ArrayList<MenuItem> items = map.get(selectedOrder);
		
		ArrayList<String> products = new ArrayList<String>();
		for(MenuItem m : items)
		{
			products.add(m.getName());
		}
		
		try
		{
			FileWriter fileWriter = new FileWriter("bill.txt");
		    PrintWriter printWriter = new PrintWriter(fileWriter);
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			printWriter.println("--Bill created at " + dtf.format(now));
			printWriter.println("Order ID: " + selectedOrder.getId());
			printWriter.println("Date: " + selectedOrder.getDate());
			printWriter.println("Table: " + selectedOrder.getTable());
			printWriter.print("Products bought: ");
			int totalCost = 0;
			for(MenuItem m : items)
			{
				printWriter.print(m.getName() + " ");
				totalCost += m.getPrice();
			}
			
			printWriter.println("\nTotal cost: " + totalCost);
			
			printWriter.close();
			assert isMapWellFormed(selectedOrder);
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}	
	}



}
