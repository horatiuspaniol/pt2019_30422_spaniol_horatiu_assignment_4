package data_layer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import business_layer.BaseProduct;
import business_layer.CompositeProduct;
import business_layer.MenuItem;
import business_layer.Order;

public class RestaurantSerializator
{
	
	public static void serialize(Object object, String fileName)
	{ 
        // Serialization  
        try
        {    
            //Saving of object in a file 
            FileOutputStream file = new FileOutputStream(fileName); 
            ObjectOutputStream out = new ObjectOutputStream(file); 
              
            // Method for serialization of object 
            out.writeObject(object); 
              
            out.close(); 
            file.close(); 
            
        } 
        catch(IOException e) 
        { 
        	System.out.println("IOException while serializing object."); 
        } 
	}
	
	public static MenuItem deserializeMenuItemFrom(String fileName)
	{
		MenuItem deserializedMenuItem = null; 
		  
        try
        {    
            // Reading the object from a file 
            FileInputStream file = new FileInputStream(fileName); 
            ObjectInputStream in = new ObjectInputStream(file); 
              
            // Method for deserialization of object 
            deserializedMenuItem = (MenuItem)in.readObject(); 
              
            in.close(); 
            file.close(); 
        } 
          
        catch(IOException ex) 
        { 
            System.out.println("IOException while deserializing"); 
        } 
          
        catch(ClassNotFoundException ex) 
        { 
            System.out.println("ClassNotFoundException while deserializing"); 
        } 
        
        return deserializedMenuItem;
	}
	
	public static ArrayList<MenuItem> deserializeListFrom(String fileName)
	{
		ArrayList<MenuItem> deserializedMenuItemList = null; 
		  
        try
        {    
            // Reading the object from a file 
            FileInputStream file = new FileInputStream(fileName); 
            ObjectInputStream in = new ObjectInputStream(file); 
              
            // Method for deserialization of object 
            deserializedMenuItemList = (ArrayList<MenuItem>)in.readObject(); 
              
            in.close(); 
            file.close(); 
        } 
          
        catch(IOException ex) 
        { 
            System.out.println("IOException while deserializing"); 
        } 
          
        catch(ClassNotFoundException ex) 
        { 
            System.out.println("ClassNotFoundException while deserializing"); 
        } 
        
        return deserializedMenuItemList;
	}
	
	
	public static ArrayList<Order> deserializeOrderListFrom(String fileName)
	{
		ArrayList<Order> deserializedMenuItemList = null; 
		  
        try
        {    
            // Reading the object from a file 
            FileInputStream file = new FileInputStream(fileName); 
            ObjectInputStream in = new ObjectInputStream(file); 
              
            // Method for deserialization of object 
            deserializedMenuItemList = (ArrayList<Order>)in.readObject(); 
              
            in.close(); 
            file.close(); 
        } 
          
        catch(IOException ex) 
        { 
            System.out.println("IOException while deserializing"); 
        } 
          
        catch(ClassNotFoundException ex) 
        { 
            System.out.println("ClassNotFoundException while deserializing"); 
        } 
        
        return deserializedMenuItemList;
	}

	public static HashMap<Order, ArrayList<MenuItem>> deserializeRestaurantFrom(String fileName)
	{
		HashMap<Order, ArrayList<MenuItem>> deserializedRestaurant = null; 
		  
        try
        {    
            // Reading the object from a file 
            FileInputStream file = new FileInputStream(fileName); 
            ObjectInputStream in = new ObjectInputStream(file); 
              
            // Method for deserialization of object 
            deserializedRestaurant = (HashMap<Order, ArrayList<MenuItem>>)in.readObject(); 
              
            in.close(); 
            file.close(); 
        } 
          
        catch(IOException ex) 
        { 
            System.out.println("IOException while deserializing"); 
        } 
          
        catch(ClassNotFoundException ex) 
        { 
            System.out.println("ClassNotFoundException while deserializing"); 
        } 
        
        return deserializedRestaurant;
	}

	
	
}
