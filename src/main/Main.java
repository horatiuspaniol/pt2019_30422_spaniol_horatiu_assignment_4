package main;

import java.util.ArrayList;
import java.util.List;

import business_layer.BaseProduct;
import business_layer.CompositeProduct;
import business_layer.MenuItem;
import business_layer.Restaurant;
import data_layer.RestaurantSerializator;
import presentation_layer.AdministratorGUI;
import presentation_layer.ChefGUI;
import presentation_layer.WaiterGUI;

public class Main
{
	
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException
	{	
		Restaurant restaurant = new Restaurant();
		ChefGUI chefGUI = new ChefGUI(restaurant);
		restaurant.addObserver(chefGUI);
	
		AdministratorGUI admin = new AdministratorGUI(restaurant);
		WaiterGUI waiter = new WaiterGUI(restaurant);
	}	
	
	
	private static void testSerializationOfList()
	{
		MenuItem meat = new BaseProduct("Meat", 50);
		MenuItem ketchup = new BaseProduct("Ketchup", 5);
		MenuItem salad = new BaseProduct("Salad", 10);
		List<MenuItem> menuItemList = new ArrayList<MenuItem>();
		menuItemList.add(meat);
		menuItemList.add(ketchup);
		menuItemList.add(salad);
		RestaurantSerializator.serialize(menuItemList, "file.ser");
	}
	
	private static void testDeserializationOfList()
	{
		List<MenuItem> menuItemList = RestaurantSerializator.deserializeListFrom("file.ser");
		for(MenuItem m : menuItemList)
		{
			System.out.println(m.getName() + " " + m.getPrice());
		}
	}
	
	private static void testSerializationOfObject()
	{
		MenuItem meat = new BaseProduct("Meat", 50);
		MenuItem ketchup = new BaseProduct("Ketchup", 5);
		MenuItem salad = new BaseProduct("Salad", 10);
		MenuItem bigMac = new CompositeProduct("Big Mac");
		((CompositeProduct) bigMac).add(meat);
		((CompositeProduct) bigMac).add(ketchup);
		((CompositeProduct) bigMac).add(salad);
		((CompositeProduct) bigMac).computePrice();
		RestaurantSerializator.serialize(bigMac, "file.ser");
	}
	
	private static void testDeserializationOfObject()
	{
		MenuItem bigMac = RestaurantSerializator.deserializeMenuItemFrom("file.ser");
		System.out.println(bigMac.getName() + " " + bigMac.getPrice() + " :");
		for(MenuItem m : ((CompositeProduct)bigMac).getMenuItems())
		{
			System.out.println(m.getName() + " " + m.getPrice());
		}
	}
	
	private static void testProductCreation()
	{
		MenuItem fries = new BaseProduct("Fries", 20);
		MenuItem meat = new BaseProduct("Meat", 50);
		MenuItem ketchup = new BaseProduct("Ketchup", 5);
		MenuItem salad = new BaseProduct("Salad", 10);
		MenuItem bigMac = new CompositeProduct("Big Mac");
		((CompositeProduct) bigMac).add(meat);
		((CompositeProduct) bigMac).add(ketchup);
		((CompositeProduct) bigMac).add(salad);
		((CompositeProduct) bigMac).computePrice();
		
		MenuItem calamari = new BaseProduct("Calamari", 15);
		MenuItem extraFries = new BaseProduct("ExtraFries", 50);
		
		MenuItem calamariMenu = new CompositeProduct("Calamari Menu");
		((CompositeProduct) calamariMenu).add(calamari);
		((CompositeProduct) calamariMenu).add(extraFries);
		((CompositeProduct) calamariMenu).computePrice();
		
		
		System.out.println("Big Mac " + bigMac.getPrice());
		System.out.println("Fries " + fries.getPrice());
		System.out.println("Calamari Menu " + calamariMenu.getPrice());
		
		
		
		MenuItem bigMacMenu = new CompositeProduct("Big Mac Menu");
		((CompositeProduct) bigMacMenu).add(fries);
		((CompositeProduct) bigMacMenu).add(bigMac);
		((CompositeProduct) bigMacMenu).add(calamariMenu);
		((CompositeProduct) bigMacMenu).computePrice();
		
		
		
		System.out.println();
		System.out.println(bigMacMenu.getName() + ":" + bigMacMenu.getPrice());
		for(MenuItem m : ((CompositeProduct) bigMacMenu).getMenuItems())
		{
			System.out.println(m.getName() + " " + m.getPrice());
		}		
	}
}
