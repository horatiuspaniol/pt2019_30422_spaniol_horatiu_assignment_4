package presentation_layer;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import business_layer.MenuItem;
import business_layer.Order;
import business_layer.Restaurant;
import data_layer.RestaurantSerializator;

public class WaiterGUI
{
	Restaurant restaurant;
	
	JFrame jframe;
	JPanel finalPanel;
		
	JPanel tablePanel;
	public static OrderTable orderTable;
	JScrollPane orderTableScroll;
	
	JPanel waiterOperationsPanel;
	JLabel waiterOperationsLabel;
	
	JPanel labelsAndTextFieldsPanel;
	JLabel idLabel;
	public static JTextField idTextField;
	JLabel dateLabel;
	public static JTextField dateTextField;
	JLabel tableLabel;
	public static JTextField tableTextField;
	
	JPanel buttonsPanel;
	JButton addButton;
	JButton createBillButton;
	
	
	JPanel addMenuItemsTitlePanel;
	JLabel addMenuLabel;
	
	JPanel addMenuItems2Panel;
	JLabel menuItemNameLabel;
	public static JTextField menuItemNameTextField;
	JButton menuItemAddButton;
	JButton finalizeButton;
	
	
	
	public WaiterGUI(Restaurant restaurant) throws IllegalArgumentException, IllegalAccessException
	{
		this.restaurant = restaurant;
		
		jframe = new JFrame("Waiter");
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.setSize(800, 600);

		tablePanel = new JPanel();
		orderTable = new OrderTable();
		orderTableScroll = new JScrollPane(orderTable);
		tablePanel.add(orderTableScroll);

		waiterOperationsPanel = new JPanel();
		waiterOperationsLabel = new JLabel("Operations");
		waiterOperationsPanel.add(waiterOperationsLabel);
		
		labelsAndTextFieldsPanel = new JPanel();
		idLabel = new JLabel("ID");
		idTextField = new JTextField();
		idTextField.setPreferredSize(new Dimension(200,25));
		dateLabel = new JLabel("Date");
		dateTextField = new JTextField();
		dateTextField.setPreferredSize(new Dimension(200,25));
		tableLabel = new JLabel("Table");
		tableTextField = new JTextField();
		tableTextField.setPreferredSize(new Dimension(200,25));
		labelsAndTextFieldsPanel.add(idLabel);
		labelsAndTextFieldsPanel.add(idTextField);
		labelsAndTextFieldsPanel.add(dateLabel);
		labelsAndTextFieldsPanel.add(dateTextField);
		labelsAndTextFieldsPanel.add(tableLabel);
		labelsAndTextFieldsPanel.add(tableTextField);
		
		buttonsPanel = new JPanel();
		addButton = new JButton("Add");
		addButton.addActionListener(new addOrderActionListener());
		createBillButton = new JButton("Create bill");
		createBillButton.addActionListener(new createBillActionListener());
		buttonsPanel.add(addButton);
		buttonsPanel.add(createBillButton);

		addMenuItemsTitlePanel = new JPanel();
		addMenuLabel = new JLabel("Add menu items to selected order.");
		addMenuItemsTitlePanel.add(addMenuLabel);
		
		
		addMenuItems2Panel = new JPanel();
		menuItemNameLabel = new JLabel("Name");
		menuItemNameTextField = new JTextField();
		menuItemNameTextField.setPreferredSize(new Dimension(200, 25));
		menuItemAddButton = new JButton("Add");
		menuItemAddButton.addActionListener(new addMenuItemActionListener());
		finalizeButton = new JButton("Finalize");
		finalizeButton.addActionListener(new finalizeActionListener());
		addMenuItems2Panel.add(menuItemNameLabel);
		addMenuItems2Panel.add(menuItemNameTextField);
		addMenuItems2Panel.add(menuItemAddButton);
		addMenuItems2Panel.add(finalizeButton);
		
		
		
		finalPanel = new JPanel();
		finalPanel.add(tablePanel);
		finalPanel.add(waiterOperationsPanel);
		finalPanel.add(labelsAndTextFieldsPanel);
		finalPanel.add(buttonsPanel);
		finalPanel.add(addMenuItemsTitlePanel);
		finalPanel.add(addMenuItems2Panel);
		
		finalPanel.setLayout(new BoxLayout(finalPanel, BoxLayout.PAGE_AXIS));
		jframe.setResizable(false);
		jframe.add(finalPanel);
		jframe.setContentPane(finalPanel);
		jframe.setVisible(true);
		
		clearTable();
		loadTableWith(restaurant.getOrders());
	}
	
	private static void removeAllRows(JTable jtable)
	{
		DefaultTableModel dm = (DefaultTableModel) jtable.getModel();
		int rowCount = dm.getRowCount();
		//Remove rows one by one from the end of the table
		for (int i = rowCount - 1; i >= 0; i--) {
		    dm.removeRow(i);
		}
	}
	
	public static void clearTable()
	{
		removeAllRows(orderTable);			
	}
	
	public static void loadTableWith(ArrayList<Order> orders)
	{
		for(Order o : orders)
		{
			orderTable.addOrder(o);
		}
	}
	
	private class addOrderActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			restaurant.addOrder();
			
			clearTable();
			loadTableWith(restaurant.getOrders());
		}	
	}
	
	
	private class addMenuItemActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			restaurant.addMenuItemToOrder();
			
			clearTable();
			loadTableWith(restaurant.getOrders());
		}
	}
	
	private class finalizeActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			restaurant.finalizeOrder();
			restaurant.notifyObservers();
		}
	}
	
	
	private class createBillActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			restaurant.createBill();
			JOptionPane.showMessageDialog(null, "Bill created succesfully!", "Bill notification", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	
	
	
	
}
