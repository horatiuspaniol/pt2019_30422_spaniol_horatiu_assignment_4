package presentation_layer;

import java.awt.Color;
import java.awt.Font;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import business_layer.Order;

public class OrderTable extends JTable
{
static DefaultTableModel model;
	
	OrderTable() throws IllegalArgumentException, IllegalAccessException
	{
		Vector<String> columns = new Vector<String>();
		columns.add("ID");
		columns.add("Date");
		columns.add("Table");
		
		
        model = new DefaultTableModel();
        model.setColumnIdentifiers(columns);
        this.setModel(model);
        
        this.setBackground(Color.LIGHT_GRAY);
        this.setForeground(Color.black);
        Font font = new Font("",1,22);
        this.setFont(font);
        this.setRowHeight(30);
	}
	
	public static void addOrder(Order order)
	{
		Object[] row = new Object[3];
		row[0] = order.getId();
		row[1] = order.getDate();
		row[2] = order.getTable();
		model.addRow(row);
	}

}
