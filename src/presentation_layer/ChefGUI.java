	package presentation_layer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import business_layer.MenuItem;
import business_layer.Order;
import business_layer.Restaurant;
import data_layer.RestaurantSerializator;

public class ChefGUI implements Observer
{
	Restaurant restaurant;
	
	public ChefGUI(Restaurant restaurant)
	{
		this.restaurant = restaurant;
	}
	
	/*
	public static void update(Order o)
	{
		restaurant.setRestaurant(RestaurantSerializator.deserializeRestaurantFrom("restaurant.ser"));
		HashMap<Order, ArrayList<MenuItem>> map = restaurant.getRestaurant();
		
		ArrayList<MenuItem> items = map.get(o);
		ArrayList<String> itemsNames = new ArrayList<String>();
		for(MenuItem m : items)
		{
			itemsNames.add(m.getName());
		}
		
		String message = "Order " + o.getId() + "has been finalized.\nThe Chef will now know to cook: ";
		for(String s : itemsNames)
		{
			message += s + " ";
		}
		
		message += " for table " + o.getTable();
		
		JOptionPane.showMessageDialog(null, message, "Chef Notification", JOptionPane.INFORMATION_MESSAGE);
	}
	*/
	
	@Override
	public void update(Observable o, Object arg)
	{
		JOptionPane.showMessageDialog(null, "Chef now knows an order is finalized!", "Chef notification", JOptionPane.INFORMATION_MESSAGE);
	}
	
}
