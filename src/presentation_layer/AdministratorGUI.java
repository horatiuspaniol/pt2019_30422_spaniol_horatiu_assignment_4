package presentation_layer;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import business_layer.BaseProduct;
import business_layer.CompositeProduct;
import business_layer.MenuItem;
import business_layer.Restaurant;
import data_layer.RestaurantSerializator;

import java.util.*;

public class AdministratorGUI
{
	Restaurant restaurant;
	
	JFrame jframe;
	JPanel finalPanel;
		
	JPanel tablePanel;
	public static MenuTable menuTable;
	JScrollPane menuTableScroll;
	
	JPanel baseProductTitlePanel;
	JLabel baseProductTitleLabel;
	
	JPanel baseProductFieldsPanel;
	JLabel baseProductNameLabel;
	public static JTextField baseProductNameTextField;
	JLabel baseProductPriceLabel;
	public static JTextField baseProductPriceTextField;

	JPanel baseProductButtonsPanel;
	JButton baseProductAddButton;
	JButton baseProductEditButton;
	JButton baseProductDeleteButton;
	
	JPanel compositeProductTitlePanel;
	JLabel compositeProductTitleLabel;
	
	JPanel compositeProductFieldsPanel;
	JLabel compositeProductNameLabel;
	public static JTextField compositeProductNameTextField;
	
	JPanel compositeProductButtonsPanel;
	JButton compositeProductCreateButton;
	JButton compositeProductAddButton;
	JButton compositeProductFinalizeButton;
	JButton compositeProductEditButton;
	JButton compositeProductDeleteButton;
	
	public AdministratorGUI(Restaurant restaurant) throws IllegalArgumentException, IllegalAccessException
	{
		this.restaurant = restaurant;
		
		jframe = new JFrame("Administrator");
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.setSize(800, 600);
	
		tablePanel = new JPanel();
		menuTable = new MenuTable();
		menuTableScroll = new JScrollPane(menuTable);
		tablePanel.add(menuTableScroll);
		
		baseProductTitlePanel = new JPanel();
		baseProductTitleLabel = new JLabel("Base product");
		baseProductTitlePanel.add(baseProductTitleLabel);
		
		baseProductFieldsPanel = new JPanel();
		baseProductNameLabel = new JLabel("Name");
		baseProductNameTextField = new JTextField();
		baseProductNameTextField.setPreferredSize(new Dimension(200, 25));
		baseProductPriceLabel = new JLabel("Price");
		baseProductPriceTextField = new JTextField();
		baseProductPriceTextField.setPreferredSize(new Dimension(200, 25));
		baseProductFieldsPanel.add(baseProductNameLabel);
		baseProductFieldsPanel.add(baseProductNameTextField);
		baseProductFieldsPanel.add(baseProductPriceLabel);
		baseProductFieldsPanel.add(baseProductPriceTextField);
		
		baseProductButtonsPanel = new JPanel();
		baseProductAddButton = new JButton("Add");
		baseProductAddButton.addActionListener(new AddBaseProductActionListener());
		baseProductEditButton = new JButton("Edit");
		baseProductEditButton.addActionListener(new EditBaseProductActionListener());
		baseProductDeleteButton = new JButton("Delete");
		baseProductDeleteButton.addActionListener(new DeleteProductActionListener());
		baseProductButtonsPanel.add(baseProductAddButton);
		baseProductButtonsPanel.add(baseProductEditButton);
		baseProductButtonsPanel.add(baseProductDeleteButton);

		
		compositeProductTitlePanel = new JPanel();
		compositeProductTitleLabel = new JLabel("Composite product");
		compositeProductTitlePanel.add(compositeProductTitleLabel);
		
		compositeProductFieldsPanel = new JPanel();
		compositeProductNameLabel = new JLabel("Name");
		compositeProductNameTextField = new JTextField();
		compositeProductNameTextField.setPreferredSize(new Dimension(200,25));
		compositeProductFieldsPanel.add(compositeProductNameLabel);
		compositeProductFieldsPanel.add(compositeProductNameTextField);
		
		compositeProductButtonsPanel = new JPanel();
		compositeProductCreateButton = new JButton("Create");
		compositeProductCreateButton.addActionListener(new CreateCompositeProductActionListener());
		compositeProductAddButton = new JButton("Add");
		compositeProductAddButton.addActionListener(new AddCompositeProductActionListener());
		compositeProductFinalizeButton = new JButton("Finalize");
		compositeProductFinalizeButton.addActionListener(new FinalizeCompositeProductActionListener());
		compositeProductEditButton = new JButton("Edit");
		compositeProductEditButton.addActionListener(new EditCompositeProductActionListener());
		compositeProductDeleteButton = new JButton("Delete");
		compositeProductDeleteButton.addActionListener(new DeleteProductActionListener());
		compositeProductButtonsPanel.add(compositeProductCreateButton);
		compositeProductButtonsPanel.add(compositeProductAddButton);
		compositeProductButtonsPanel.add(compositeProductFinalizeButton);
		compositeProductButtonsPanel.add(compositeProductEditButton);
		compositeProductButtonsPanel.add(compositeProductDeleteButton);
		
		
		finalPanel = new JPanel();
		finalPanel.add(tablePanel);
		finalPanel.add(baseProductTitlePanel);
		finalPanel.add(baseProductFieldsPanel);
		finalPanel.add(baseProductButtonsPanel);
		finalPanel.add(compositeProductTitlePanel);
		finalPanel.add(compositeProductFieldsPanel);
		finalPanel.add(compositeProductButtonsPanel);
		
		
		finalPanel.setLayout(new BoxLayout(finalPanel, BoxLayout.PAGE_AXIS));
		jframe.setResizable(false);
		jframe.add(finalPanel);
		jframe.setContentPane(finalPanel);
		jframe.setVisible(true);
		
		clearTable();
		ArrayList<MenuItem> tempMenu = RestaurantSerializator.deserializeListFrom("menu.ser");
		loadTableWith(tempMenu);
	}
	
	private static void removeAllRows(JTable jtable)
	{
		DefaultTableModel dm = (DefaultTableModel) jtable.getModel();
		int rowCount = dm.getRowCount();
		//Remove rows one by one from the end of the table
		for (int i = rowCount - 1; i >= 0; i--) {
		    dm.removeRow(i);
		}
	}
	
	public static void clearTable()
	{
		removeAllRows(menuTable);			
	}
	
	public static void loadTableWith(ArrayList<MenuItem> menu)
	{
		for(MenuItem m : menu)
		{
			menuTable.addMenuItem(m);
		}
	}
	
	private class AddBaseProductActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			restaurant.addBaseProduct();
			
			clearTable();
			loadTableWith(restaurant.getMenu());
		}
	}
	
	private class EditBaseProductActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			restaurant.editBaseProduct();
			
			clearTable();
			loadTableWith(restaurant.getMenu());
			
		}
	}
	
	private class DeleteProductActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			restaurant.deleteMenuItem();
			
			clearTable();
			loadTableWith(restaurant.getMenu());
		}
	}
	
	private class CreateCompositeProductActionListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			restaurant.createCompositeProduct();
			
			clearTable();
			loadTableWith(restaurant.getMenu());
		}
	}
	
	private class AddCompositeProductActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			restaurant.addCompositeProduct();
			
			clearTable();
			loadTableWith(restaurant.getMenu());
		}
	}
	
	private class FinalizeCompositeProductActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			restaurant.finalizeCompositeProduct();
			
			clearTable();
			loadTableWith(restaurant.getMenu());
		}
	}	
	
	private class EditCompositeProductActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			restaurant.editCompositeProduct();
			
			clearTable();
			loadTableWith(restaurant.getMenu());
		}
	}
	
	
}
