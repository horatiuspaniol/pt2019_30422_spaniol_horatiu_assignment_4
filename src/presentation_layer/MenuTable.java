package presentation_layer;

import java.awt.Color;
import java.awt.Font;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import business_layer.MenuItem;

public class MenuTable extends JTable
{
	static DefaultTableModel model;
	
	MenuTable() throws IllegalArgumentException, IllegalAccessException
	{
		Vector<String> columns = new Vector<String>();
		columns.add("Name");
		columns.add("Price");
		
		
        model = new DefaultTableModel();
        model.setColumnIdentifiers(columns);
        this.setModel(model);
        
        this.setBackground(Color.LIGHT_GRAY);
        this.setForeground(Color.black);
        Font font = new Font("",1,22);
        this.setFont(font);
        this.setRowHeight(30);
	}
	
	public static void addMenuItem(MenuItem menuItem)
	{
		Object[] row = new Object[2];
		row[0] = menuItem.getName();
		row[1] = menuItem.getPrice();
		model.addRow(row);
	}
}
